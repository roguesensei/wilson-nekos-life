import discord
import wilson.util.helpers as h

from discord.ext import commands
from wilson.bot import Wilson
from wilson.util.http import Http

http = Http('https://nekos.life/api/v2')


def get(url: str):
    r = http.get(url)
    return r.json()


class NekosLife(commands.Cog):
    def __init__(self, bot: Wilson):
        self._bot = bot

    @commands.command(aliases=['8ball', 'fortune'])
    async def ball(self, ctx: commands.Context, *, message: str = ''):
        if message == '':
            message = 'The 8 ball'

        img = get('/8ball')['url']
        embed = self._bot.generate_embed(h.add_escape_characters(message), ctx.author, image_url=img)
        await ctx.send(embed=embed)

    @commands.command()
    async def cuddle(self, ctx: commands.Context, user: discord.Member = None):
        subject = 'themselves'
        if user is not None:
            subject = h.add_escape_characters(user.display_name)
        action = f'**{h.add_escape_characters(ctx.author.display_name)}** cuddles **{subject}**'

        img = get('/img/cuddle')['url']
        embed = self._bot.generate_embed(action, ctx.author, image_url=img)
        await ctx.send(embed=embed)

    @commands.command()
    async def hug(self, ctx: commands.Context, user: discord.Member = None):
        subject = 'themselves'
        if user is not None:
            subject = h.add_escape_characters(user.display_name)
        action = f'**{h.add_escape_characters(ctx.author.display_name)}** hugs **{subject}**'

        img = get('/img/hug')['url']
        embed = self._bot.generate_embed(action, ctx.author, image_url=img)
        await ctx.send(embed=embed)

    @commands.command(aliases=['mwah', 'smooch'])
    async def kiss(self, ctx: commands.Context, user: discord.Member = None):
        subject = 'themselves'
        if user is not None:
            subject = h.add_escape_characters(user.display_name)
        action = f'**{h.add_escape_characters(ctx.author.display_name)}** kisses **{subject}** (mwah!)'

        img = get('/img/kiss')['url']
        embed = self._bot.generate_embed(action, ctx.author, image_url=img)
        await ctx.send(embed=embed)

    @commands.command(aliases=['pap', 'pet'])
    async def pat(self, ctx: commands.Context, user: discord.Member = None):
        subject = 'themselves'
        if user is not None:
            subject = h.add_escape_characters(user.display_name)
        action = f'**{h.add_escape_characters(ctx.author.display_name)}** pats **{subject}**'

        img = get('/img/pat')['url']
        embed = self._bot.generate_embed(action, ctx.author, image_url=img)
        await ctx.send(embed=embed)

    @commands.command(aliases=['smack', 'bitch'])
    async def slap(self, ctx: commands.Context, user: discord.Member = None):
        subject = 'themselves'
        if user is not None:
            subject = h.add_escape_characters(user.display_name)
        action = f'**{h.add_escape_characters(ctx.author.display_name)}** slaps **{subject}**'

        img = get('/img/slap')['url']
        embed = self._bot.generate_embed(action, ctx.author, image_url=img)
        await ctx.send(embed=embed)

    @commands.command()
    async def smug(self, ctx: commands.Context, user: discord.Member = None):
        subject = 'the discord mods'
        if user is not None:
            subject = h.add_escape_characters(user.display_name)
        action = f'**{h.add_escape_characters(ctx.author.display_name)}** is smug toward **{subject}**'

        img = get('/img/smug')['url']
        embed = self._bot.generate_embed(action, ctx.author, image_url=img)
        await ctx.send(embed=embed)

    @commands.command()
    async def tickle(self, ctx: commands.Context, user: discord.Member = None):
        subject = 'themselves'
        if user is not None:
            subject = h.add_escape_characters(user.display_name)
        action = f'**{h.add_escape_characters(ctx.author.display_name)}** tickles **{subject}**'

        img = get('/img/tickle')['url']
        embed = self._bot.generate_embed(action, ctx.author, image_url=img)
        await ctx.send(embed=embed)


async def setup(bot: Wilson):
    await bot.add_cog(NekosLife(bot))
