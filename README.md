# Wilson Extension: Nekos.Life
Adds commands that utilize a few of the endpoints from the [Nekos.Life](https://nekos.life/) API.
